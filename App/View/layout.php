<?
/* @var string $content */
/* @var string $controller */
/* @var string $action */
/* @var \Framework\Abstracts\AbstractController $this */

?>
<html>
<head>
	<title><?= $controller ?>/<?= $action ?></title>
	<link rel="stylesheet" href="/web/style.css">
</head>
<header>
	<? if (!$this->engine->identity()->isGuest()): ?>
		<a href="/?controller=file">Files</a>
		<a href="/?controller=login&action=profile">Profile</a>
		<a href="/?controller=login&action=logout">Logout</a>
	<? else: ?>
		<a href="/?controller=login&action=register">Register</a>
		<a href="/">Home</a>
	<? endif ?>
</header>
<body>
<div class="container container-<?= $controller ?>-<?= $action ?>">
	<?= $content ?>
</div>
</body>
</html>