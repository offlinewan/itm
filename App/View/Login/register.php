<?
/* @var $form \App\Form\RegisterForm */
?>
<form action="/" method="POST">
	<input type="hidden" name="action" value="register">
	<input type="hidden" name="controller" value="login">
	<table class="w100p">
		<tr>
			<td>
				<label for="varLogin">Login<span class="important">*</span></label>
			</td>
			<td>
				<input class="w100p" required type="text" id="varLogin" name="varLogin" value="<?= $form->varLogin ?>"
					   placeholder="Login"/>
			</td>
		</tr>

		<tr>
			<td>
				<label for="varName">Name<span class="important">*</span></label>
			</td>
			<td>
				<input class="w100p" required type="text" id="varName" name="varName" value="<?= $form->varName ?>"
					   placeholder="Name"/>
			</td>
		</tr>

		<tr>
			<td>
				<label for="varLastName">Last Name<span class="important">*</span></label>
			</td>
			<td>
				<input class="w100p" required type="text" id="varLastName" name="varLastName"
					   value="<?= $form->varLastName ?>"
					   placeholder="Last Name">
			</td>
		</tr>

		<tr>
			<td>
				<label for="varPassword">Password<span class="important">*</span></label>
			</td>
			<td>
				<input class="w100p" required type="password" id="varPassword" name="varPassword"
					   value="<?= $form->varPassword ?>"
					   placeholder="Password">
			</td>
		</tr>

		<tr>
			<td>
				<label for="varEmail">Email</label>
			</td>
			<td>
				<input class="w100p" type="email" id="varEmail" name="varEmail" value="<?= $form->varEmail ?>"
					   placeholder="Email">
			</td>
		</tr>

		<tr>
			<td>
				<label for="varPhone">Phone</label>
			</td>
			<td>
				<input class="w100p" type="tel" id="varPhone" name="varPhone" value="<?= $form->varPhone ?>"
					   placeholder="Phone">
			</td>
		</tr>

		<tr>
			<td>
				<label>Birth Date</label>
			</td>
			<td>
				<input class="w33p" type="number" id="intBirthDay" name="intBirthDay" min="1" max="31"
					   value="<?= $form->intBirthDay ?>" placeholder="Day">
				<input class="w33p" type="number" id="intBirthMonth" name="intBirthMonth" min="1" max="12"
					   value="<?= $form->intBirthMonth ?>" placeholder="Month">
				<input class="w33p" type="number" id="intBirthYear" name="intBirthYear" min="1900"
					   max="<?= date('Y') ?>" value="<?= $form->intBirthYear ?>" placeholder="Year">
			</td>
		</tr>

		<tr>
			<td>
			</td>
			<td><input class="w100p" type="submit" value="Register"/></td>
		</tr>
	</table>
	<? if ($form->hasErrors()): ?>
		<div class="form-errors">
			<? foreach ($form->getErrors() as $message): ?>
				<li><?= $message ?></li>
			<? endforeach ?>
		</div>
	<? endif ?>
</form>