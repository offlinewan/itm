<?
/* @var $form \App\Form\RegisterForm */
/* @var $this \Framework\Abstracts\AbstractController */
?>
	<form action="/" method="POST">
		<input type="hidden" name="action" value="profile">
		<input type="hidden" name="controller" value="login">
		<table class="w100p">
			<tr>
				<td>
					<label for="varLogin">Login<span class="important">*</span></label>
				</td>
				<td>
					<input readonly="readonly" class="w100p" required type="text" id="varLogin"
						   value="<?= $this->engine->identity()->get()->getLogin() ?>"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="varName">Name<span class="important">*</span></label>
				</td>
				<td>
					<input class="w100p" required type="text" id="varName" name="varName" placeholder="Name"
						   value="<?= $form->varName ?>"/>
				</td>
			</tr>

			<tr>
				<td>
					<label for="varLastName">Last Name<span class="important">*</span></label>
				</td>
				<td>
					<input class="w100p" required type="text" id="varLastName" name="varLastName"
						   placeholder="Last Name"
						   value="<?= $form->varLastName ?>">
				</td>
			</tr>

			<tr>
				<td>
					<label for="varPassword">Password</label>
				</td>
				<td>
					<input class="w100p" type="password" id="varPassword" name="varPassword" placeholder="•••••"
						   value="<?= $form->varPassword ?>">
				</td>
			</tr>

			<tr>
				<td>
					<label for="varEmail">Email</label>
				</td>
				<td>
					<input class="w100p" type="email" id="varEmail" name="varEmail" value="<?= $form->varEmail ?>"
						   placeholder="Email">
				</td>
			</tr>

			<tr>
				<td>
					<label for="varPhone">Phone</label>
				</td>
				<td>
					<input class="w100p" type="tel" id="varPhone" name="varPhone" value="<?= $form->varPhone ?>"
						   placeholder="Phone">
				</td>
			</tr>

			<tr>
				<td>
					<label>Birth Date</label>
				</td>
				<td>
					<input class="w33p" type="number" id="intBirthDay" name="intBirthDay" min="1" max="31"
						   value="<?= $form->intBirthDay ?>" placeholder="Day">
					<input class="w33p" type="number" id="intBirthMonth" name="intBirthMonth" min="1" max="12"
						   value="<?= $form->intBirthMonth ?>" placeholder="Month">
					<input class="w33p" type="number" id="intBirthYear" name="intBirthYear" min="1900"
						   max="<?= date('Y') ?>" value="<?= $form->intBirthYear ?>" placeholder="Year">
				</td>
			</tr>

			<tr>
				<td>

				</td>
				<td>
					<input class="w100p" type="submit" value="Save"/>
				</td>
			</tr>
		</table>
	</form>

<? if ($form->hasErrors()): ?>
	<div class="form-errors">
		<? foreach ($form->getErrors() as $message): ?>
			<li><?= $message ?></li>
		<? endforeach ?>
	</div>
<? endif ?>