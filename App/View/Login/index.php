<?
/* @var $form \App\Form\LoginForm */
?>
	<form action="/" method="POST">
		<input type="hidden" name="controller" value="login">
		<input type="hidden" name="action" value="index">
		<table class="w100p">
			<tr>
				<td>
					<label for="varLogin">Login</label>
				</td>
				<td>
					<input class="w100p" required type="text" placeholder="Login" id="varLogin" name="varLogin"
						   value="<?= $form->varLogin ?>"/>
				</td>
			</tr>

			<tr>
				<td>
					<label for="varPassword">Password</label>
				</td>
				<td>
					<input class="w100p" required type="password" placeholder="Password" id="varPassword"
						   name="varPassword" value="<?= $form->varPassword ?>">
				</td>
			</tr>

			<tr>
				<td>
				</td>
				<td>
					<input class="w100p" type="submit" value="Login"/>
				</td>
			</tr>
		</table>
	</form>
<? if ($form->hasErrors()): ?>
	<div class="form-errors">
		<? foreach ($form->getErrors() as $message): ?>
			<li><?= $message ?></li>
		<? endforeach ?>
	</div>
<? endif ?>