<?
/* @var $files \App\Model\FileModel[] */
/* @var $form \App\Form\FileForm */
?>
<form method="post" enctype="multipart/form-data" action="/">
	<input type="hidden" name="action" value="">
	<input type="hidden" name="controller" value="file">
	<input type="file" required name="file">
	<input type="submit" value="Upload">
	<? if ($form->hasErrors()): ?>
		<div class="form-errors">
			<? foreach ($form->getErrors() as $message): ?>
				<li><?= $message ?></li>
			<? endforeach ?>
		</div>
	<? endif ?>
</form>
<table class="w100p">
	<thead>
	<tr>
		<th>#</th>
		<th>Filename</th>
		<th style="width: 150px;"></th>
	</tr>
	</thead>
	<tbody>
	<? $counter = 0; ?>
	<? foreach ($files as $file): ?>
		<tr>
			<td><?= ++$counter; ?></td>
			<td>
				<?= $file->varName; ?>
			</td>
			<td>
				<a href="/?controller=file&action=download&id=<?= $file->intFileID; ?>">Download</a>
				<a href="/?controller=file&action=delete&id=<?= $file->intFileID; ?>">Remove</a>
			</td>
		</tr>
	<? endforeach ?>
	</tbody>
</table>