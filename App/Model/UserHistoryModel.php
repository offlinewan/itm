<?php

namespace App\Model;

use Framework\Abstracts\AbstractModel;
use Framework\Engine;

/**
 * Class FileModel
 */
class UserHistoryModel extends AbstractModel
{
	const table = 'user_history';
	const pk = 'intUserHistoryID';

	public $intUserHistoryID, $intUserID, $jsonHistory;

	/**
	 * @param UserModel $userModel
	 * @return static
	 */
	public static function crateFromUser(UserModel $userModel)
	{
		$diff = [];
		/* @var $identity UserModel */
		$identity = Engine::getInstance()->identity()->get();
		foreach ($userModel as $field => $value)
		{
			if ($userModel->$field != $identity->$field)
			{
				$old = $identity->$field;
				if ($identity->$field instanceof \DateTime)
				{
					$old = $identity->$field->format('Y-m-d');
				}

				$new = $userModel->$field;
				if ($userModel->$field instanceof \DateTime)
				{
					$new = $userModel->$field->format('Y-m-d');
				}

				$diff[$field] = ['old' => $old, 'new' => $new];
			}
		}

		$userHistory = new static();
		$userHistory->intUserID = $identity->intUserID;
		$userHistory->jsonHistory = json_encode($diff);

		return $userHistory;
	}
}