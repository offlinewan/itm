<?php

namespace App\Model;

use Framework\Abstracts\AbstractModel;

/**
 * Class FileModel
 */
class FileModel extends AbstractModel
{
	const table = 'file';
	const pk = 'intFileID';

	const STATUS_ACTIVE = 1;
	const STATUS_DELETED = 9;

	public $intFileID, $intUserID, $varName, $varFile, $intStatus;

	/**
	 * @return bool
	 */
	public function delete()
	{
		$this->intStatus = self::STATUS_DELETED;
		unlink(STORAGE_DIR . $this->varFile);

		return $this->update();
	}
}