<?php

namespace App\Model;

use Framework\Abstracts\AbstractModel;
use Framework\Abstracts\IdentityInterface;

/**
 * Class UserModel
 */
class UserModel extends AbstractModel implements IdentityInterface
{
	const table = 'user';
	const pk = 'intUserID';

	public $intUserID, $varPassword, $varName, $varLogin, $varLastName, $varEmail, $varPhone;
	/* @var $dateBirth \DateTime */
	public $dateBirth;

	/**
	 * @return mixed
	 */
	public function getID()
	{
		return $this->intUserID;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->varName;
	}

	/**
	 * @return mixed
	 */
	public function getLogin()
	{
		return $this->varLogin;
	}


	/**
	 * @return static
	 */
	public function getByLogPass()
	{
		return $this->getOne(['varLogin' => $this->varLogin, 'varPassword' => md5($this->varPassword)]);
	}
}