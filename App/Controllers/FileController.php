<?php

namespace App\Controllers;

use App\Form\FileForm;
use App\Model\FileModel;
use Framework\Abstracts\AbstractController;
use Framework\Exceptions\ForbiddenException;
use Framework\Exceptions\NotFoundException;

/**
 * Class FileController
 * @package App\Controllers
 */
class FileController extends AbstractController
{
	protected $viewPath = APP_DIR . DS . 'View' . DS . 'File' . DS;

	/**
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		if ($this->engine->identity()->isGuest())
		{
			throw new ForbiddenException();
		}
	}

	public function actionDelete()
	{
		$intFileID = (int)$this->engine->request()->get('id');
		$file = FileModel::getOne([
			'intUserID' => $this->engine->identity()->getId(),
			'intFileID' => $intFileID,
			'intStatus' => FileModel::STATUS_ACTIVE,
		]);

		if (!$file)
		{
			throw new NotFoundException;
		}

		$file->delete();

		$this->redirect('/?controller=file');
	}

	public function actionDownload()
	{
		$intFileID = (int)$this->engine->request()->get('id');
		$file = FileModel::getOne([
			'intUserID' => $this->engine->identity()->getId(),
			'intFileID' => $intFileID,
			'intStatus' => FileModel::STATUS_ACTIVE,
		]);

		if (!$file)
		{
			throw new NotFoundException;
		}
		$attachment_location = STORAGE_DIR . $file->varFile;
		if (file_exists($attachment_location))
		{
			header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
			header("Cache-Control: public"); // needed for i.e.
			header("Content-Type: application/zip");
			header("Content-Transfer-Encoding: Binary");
			header("Content-Length:" . filesize($attachment_location));
			header("Content-Disposition: attachment; filename=" . $file->varName);
			readfile($attachment_location);
			die();
		}
		else
		{
			throw new ForbiddenException;
		}
	}

	public function actionIndex()
	{
		$files = FileModel::getMany([
			'intUserID' => $this->engine->identity()->getId(),
			'intStatus' => FileModel::STATUS_ACTIVE
		]);
		$form = new FileForm();

		if ($form->loadFromPost() && $form->validate())
		{
			$file = $form->getModel();

			if (count($files) + 1 > MAX_USER_FILES)
			{
				$form->addErrors(null, sprintf("Only %s files is allowed for one user.", MAX_USER_FILES));
			}
			else
			{
				$file->insert();
				$this->redirect('/?controller=file');
			}
		}

		$this->templateVars['files'] = $files;
		$this->templateVars['form'] = $form;
		$this->display('index.php');
	}
}