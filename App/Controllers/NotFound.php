<?php
namespace App\Controllers;

use Framework\Abstracts\AbstractController;

/**
 * Class NotFound
 * @package App\Controllers
 */
class NotFound extends AbstractController
{
	public function actionIndex()
	{
		$this->display('notFound.php');
	}
}