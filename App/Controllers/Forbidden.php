<?php
namespace App\Controllers;

use Framework\Abstracts\AbstractController;

/**
 * Class NotFound
 * @package App\Controllers
 */
class Forbidden extends AbstractController
{
	public function actionIndex()
	{
		$this->display('forbidden.php');
	}
}