<?php

namespace App\Controllers;

use App\Form\LoginForm;
use App\Form\ProfileForm;
use App\Form\RegisterForm;
use App\Model\UserHistoryModel;
use App\Model\UserModel;
use Framework\Abstracts\AbstractController;
use Framework\Exceptions\DbException;
use Framework\Exceptions\ForbiddenException;

/**
 * Class LoginController
 */
class LoginController extends AbstractController
{
	protected $viewPath = APP_DIR . DS . 'View' . DS . 'Login' . DS;

	public function actionRegister()
	{
		if (!$this->engine->identity()->isGuest())
		{
			throw new ForbiddenException();
		}

		$form = new RegisterForm();
		if ($form->loadFromPost() && $form->validate())
		{
			$user = $form->getModel();
			try
			{
				$user->insert();
				$this->engine->identity()->login($user);
				$this->redirect('/');
			}
			catch (DbException $e)
			{
				var_dump($e->getMessage());
			}
		}

		$this->templateVars['form'] = $form;

		$this->display('register.php');
	}

	public function actionIndex()
	{
		if (!$this->engine->identity()->isGuest())
		{
			throw new ForbiddenException();
		}

		$form = new LoginForm();
		if ($form->loadFromPost() && $form->validate())
		{
			$user = $form->getModel();
			$dbUser = $user->getByLogPass();
			if ($dbUser)
			{
				$this->engine->identity()->login($dbUser);
				$this->redirect('/');
			}
			$form->addErrors(null, "Wrong login or password.");
		}

		$this->templateVars['form'] = $form;
		$this->display('index.php');
	}

	public function actionProfile()
	{
		if ($this->engine->identity()->isGuest())
		{
			throw new ForbiddenException();
		}
		/* @var $user UserModel */
		$user = $this->engine->identity()->get();
		$form = new ProfileForm();
		$form->fillFromObject($user);

		if ($user->dateBirth)
		{
			$form->intBirthYear = $user->dateBirth->format('Y');
			$form->intBirthMonth = $user->dateBirth->format('m');
			$form->intBirthDay = $user->dateBirth->format('d');
		}

		$form->varPassword = null;

		if ($form->loadFromPost() && $form->validate())
		{
			$user = $form->getModel();

			$userHistory = UserHistoryModel::crateFromUser($user);

			try
			{
				$user->update();
				$userHistory->insert();
				$this->engine->identity()->login($user);
				$this->redirect('/?controller=login&action=profile');
			}
			catch (DbException $e)
			{
				var_dump($e->getMessage());
			}
		}

		$this->templateVars['form'] = $form;

		$this->display('profile.php');
	}

	public function actionLogout()
	{
		if ($this->engine->identity()->isGuest())
		{
			throw new ForbiddenException();
		}

		$this->engine->identity()->logout();
		$this->redirect('/');
	}
}