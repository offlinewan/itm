<?php
namespace App\Form;

use App\Model\UserModel;
use Framework\Engine;

/**
 * Class LoginController
 * @package App\Form
 *
 */
class ProfileForm extends RegisterForm
{
	/**
	 * @return UserModel
	 */
	public function getModel()
	{
		$engine = Engine::getInstance();

		$this->varLogin = $engine->identity()->get()->getLogin();

		$user = new UserModel();
		$user->fillFromObject($this);
		$user->dateBirth = $this->fillDateFromYMD(true, $this->intBirthYear, $this->intBirthMonth, $this->intBirthDay);
		$user->varPassword = md5($this->varPassword);
		$user->intUserID = $engine->identity()->getId();

		if (empty($this->varPassword))
		{
			$user->varPassword = $engine->identity()->get()->varPassword; //it's bonus - not all perfect
		}

		return $user;
	}

	/**
	 * @return array
	 */
	protected function validators()
	{
		$validators = parent::validators();
		unset($validators['varPassword']);
		unset($validators['varLogin']);

		return $validators;
	}
}