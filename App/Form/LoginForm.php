<?php

namespace App\Form;

use App\Model\UserModel;
use Framework\Abstracts\AbstractForm;
use Framework\Validators\LoginValidator;
use Framework\Validators\NotEmptyValidator;

/**
 * Class LoginForm
 * @package App\Form
 */
class LoginForm extends AbstractForm
{
	public $varLogin, $varPassword;

	/**
	 * @return UserModel
	 */
	public function getModel()
	{
		$user = new UserModel();
		$user->fillFromObject($this);

		return $user;
	}

	/**
	 * @return array
	 */
	protected function validators()
	{
		return [
			'varLogin' => [
				new NotEmptyValidator(),
				new LoginValidator()
			],
			'varPassword' => [
				new NotEmptyValidator()
			],
		];
	}

}