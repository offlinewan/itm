<?php
namespace App\Form;

use App\Model\UserModel;
use Framework\Abstracts\AbstractForm;
use Framework\Validators\EmailValidator;
use Framework\Validators\LengthValidator;
use Framework\Validators\LoginValidator;
use Framework\Validators\MinMaxValidator;
use Framework\Validators\NotEmptyValidator;
use Framework\Validators\StringValidator;
use Framework\Validators\UniqueValidator;

/**
 * Class LoginController
 * @package App\Form
 *
 */
class RegisterForm extends AbstractForm
{
	public $varName, $varLastName, $varPhone, $varLogin, $varPassword, $varEmail, $intBirthDay, $intBirthMonth, $intBirthYear;

	/**
	 * @return UserModel
	 */
	public function getModel()
	{
		$user = new UserModel();
		$user->fillFromObject($this);
		$user->varLogin = strtolower($this->varLogin);
		$user->dateBirth = $this->fillDateFromYMD(true, $this->intBirthYear, $this->intBirthMonth, $this->intBirthDay);
		$user->varPassword = md5($this->varPassword);

		return $user;
	}

	/**
	 * @return array
	 */
	protected function validators()
	{
		return [
			'varLogin' => [
				new NotEmptyValidator(),
				new LoginValidator(),
				new LengthValidator(5, 32),
				new UniqueValidator(UserModel::class, 'varLogin'),
			],
			'varPassword' => [
				new NotEmptyValidator(),
			],
			'varName' => [
				new NotEmptyValidator(),
				new StringValidator(),
				new LengthValidator(2, 32),
			],
			'varLastName' => [
				new StringValidator(),
				new NotEmptyValidator(),
				new LengthValidator(2, 32),
			],
			'varEmail' => [
				new EmailValidator(),
			],
			'intBirthDay' => [new MinMaxValidator(1, 31)],
			'intBirthMonth' => [new MinMaxValidator(1, 12)],
			'intBirthYear' => [new MinMaxValidator(1900, date('Y'))]
		];
	}
}