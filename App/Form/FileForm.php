<?php

namespace App\Form;

use App\Model\FileModel;
use Framework\Abstracts\AbstractForm;
use Framework\Engine;
use Framework\Validators\FileSizeValidator;
use Framework\Validators\NotEmptyValidator;

/**
 * Class LoginForm
 * @package App\Form
 */
class FileForm extends AbstractForm
{
	public $file;

	/**
	 * @return FileModel
	 */
	public function getModel()
	{
		$file = new FileModel();
		$file->intUserID = Engine::getInstance()->identity()->getId();
		$file->varName = $this->file['name'];
		$file->intStatus = FileModel::STATUS_ACTIVE;
		$file->varFile = md5(time() . mt_rand(1000, 9999));

		move_uploaded_file($this->file['tmp_name'], STORAGE_DIR . $file->varFile);

		return $file;
	}

	/**
	 * @return array
	 */
	protected function validators()
	{
		return [
			'file' => [
				new NotEmptyValidator(),
				new FileSizeValidator(MAX_FILE_SIZE),
			],
		];
	}

}