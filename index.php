<?php
include 'config.php';
spl_autoload_register();

$app = \Framework\Engine::getInstance();

$controllerName = ucwords($app->request()->request('controller'));
if (empty($controllerName))
{
	if ($app->identity()->isGuest())
	{
		$controllerName = 'Login';
	}
	else
	{
		$controllerName = 'File';
	}
}
$action = ucwords($app->request()->request('action'));

try
{
	$controllerName = '\App\Controllers\\' . $controllerName . 'Controller';
	$controller = new $controllerName();
	$app->processController($controller, $action);
}
catch (\Framework\Exceptions\NotFoundException $e)
{
	$controller = new \App\Controllers\NotFound();
	$app->processController($controller);
}
catch (\Framework\Exceptions\ForbiddenException $e)
{
	$controller = new \App\Controllers\Forbidden();
	$app->processController($controller);
}
catch (\Exception $e)
{
	var_dump($e->getMessage());
}