-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `intUserID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `varPassword` varchar(255) NOT NULL,
  `varName` varchar(255) NOT NULL,
  `varLogin` varchar(255) NOT NULL,
  `varLastName` varchar(255) NOT NULL,
  `varEmail` varchar(255) DEFAULT NULL,
  `dateBirth` timestamp NULL DEFAULT NULL,
  `varPhone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`intUserID`),
  UNIQUE KEY `varLogin` (`varLogin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user_history
-- ----------------------------
DROP TABLE IF EXISTS `user_history`;
CREATE TABLE `user_history` (
  `intUserHistoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `intUserID` int(11) unsigned NOT NULL,
  `jsonHistory` text NOT NULL,
  `dateCreated` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`intUserHistoryID`),
  KEY `intUserID` (`intUserID`),
  CONSTRAINT `user_history_ibfk_1` FOREIGN KEY (`intUserID`) REFERENCES `user` (`intUserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for file
-- ----------------------------
DROP TABLE IF EXISTS `file`;
CREATE TABLE `file` (
  `intFileID` int(11) NOT NULL AUTO_INCREMENT,
  `intUserID` int(10) unsigned NOT NULL,
  `varName` varchar(255) NOT NULL,
  `varFile` varchar(255) NOT NULL,
  `intStatus` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`intFileID`),
  KEY `intUserID` (`intUserID`),
  CONSTRAINT `file_ibfk_1` FOREIGN KEY (`intUserID`) REFERENCES `user` (`intUserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
