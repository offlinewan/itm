<?php
namespace Framework\Helpers\Traits;

use Framework\Engine;

/**
 * Class FillTrait
 * @package Framework\Helpers\Traits
 */
trait FillTrait
{
	public function fillFromPost()
	{
		$request = Engine::getInstance()->request();

		if ($request->isPost())
		{
			$this->fillFromArray($request->post());
		}
	}

	/**'
	 * @param array $data
	 */
	public function fillFromArray(array $data)
	{
		foreach ($this as $field => $value)
		{
			if (isset($data[$field]))
			{
				$this->$field = $this->prepareFieldData($field, $data);
			}
		}
	}

	/**
	 * @param $field
	 * @param $data
	 * @return int|string
	 */
	private function prepareFieldData($field, $data)
	{
		if (strpos($field, 'var') === 0)
		{
			return (string)trim(strip_tags($data[$field]));
		}
		if (strpos($field, 'json') === 0)
		{
			return (string)trim(strip_tags($data[$field]));
		}
		elseif (strpos($field, 'int') === 0)
		{
			if ($data[$field] === null || $data[$field] === '')
			{
				return null;
			}
			else
			{
				return (int)$data[$field];
			}
		}
		elseif (strpos($field, 'date') === 0)
		{
			return new \DateTime($data[$field]);
		}
		elseif (strpos($field, 'file') === 0)
		{
			return $data[$field];
		}
		else
		{
			throw new \LogicException("Bad field name/type");
		}
	}

	/**
	 * @param $data
	 */
	public function fillFromObject($data)
	{
		foreach ($this as $field => $value)
		{
			if (isset($data->$field))
			{
				$this->$field = $data->$field;
			}
		}
	}

	/**
	 * @param bool|false $strict
	 * @param $intYear
	 * @param $intMonth
	 * @param $intDay
	 * @return \DateTime|null
	 */
	public function fillDateFromYMD($strict = false, $intYear, $intMonth, $intDay)
	{
		if ($strict && (empty((int)$intYear) || empty((int)$intMonth) || empty((int)$intDay)))
		{
			return null;
		}
		$dateBirth = new \DateTime();
		$dateBirth->setDate((int)$intYear, (int)$intMonth, (int)$intDay);

		return $dateBirth;
	}
}