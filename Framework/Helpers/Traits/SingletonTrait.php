<?php

namespace Framework\Helpers\Traits;

/**
 * Class SingletonTrait
 */
trait SingletonTrait
{
	protected static $instance;

	/**
	 *
	 */
	protected function __construct()
	{
	}

	/**
	 * @return static
	 */
	final public static function getInstance()
	{
		if (!isset(static::$instance))
		{
			static::$instance = new static;
		}

		return static::$instance;
	}

	final private function __wakeup()
	{
	}

	final private function __clone()
	{
	}
}