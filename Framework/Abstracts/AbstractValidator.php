<?php
namespace Framework\Abstracts;
/**
 * Class AbstractValidator
 * @package Framework\Abstracts
 */
abstract class AbstractValidator
{
	/**
	 * @param $subject
	 * @return bool
	 */
	public function validate($subject)
	{
		return $this->rule($subject);
	}

	/**
	 * @param $subject
	 * @return mixed
	 */
	abstract protected function rule($subject);

	/**
	 * @param $field
	 * @return string
	 */
	public function validationErrorMessage($field)
	{
		return sprintf("Please check %s field value.", $field);
	}
}