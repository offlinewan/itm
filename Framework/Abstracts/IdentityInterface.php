<?php

namespace Framework\Abstracts;

/**
 * Interface IdentityInterface
 */
interface IdentityInterface
{
	public function getID();

	public function getName();

	public function getLogin();
}