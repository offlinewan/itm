<?php

namespace Framework\Abstracts;

use Framework\Engine;

/**]
 * Class AbstractController
 * @package Framework
 */
abstract class AbstractController
{
	protected $layoutPath = APP_DIR . DS . 'View' . DS;
	protected $viewPath = APP_DIR . DS . 'View' . DS;

	/* @var $engine Engine */
	protected $engine;

	protected $templateVars = [];
	protected $view;
	protected $layout = 'layout.php';

	/**
	 *
	 */
	public function __construct()
	{
		$this->engine =& Engine::getInstance();
	}

	/**
	 * @param $field
	 * @param $value
	 */
	public function addToTemplate($field, $value)
	{
		$this->templateVars[$field] = $value;
	}

	/**
	 * @param $url
	 */
	public function redirect($url)
	{
		header(sprintf('Location: %s', $url));
		exit;
	}

	/**
	 * @param $view
	 */
	public function display($view)
	{
		extract($this->templateVars);

		ob_start();
		include $this->viewPath . $view;
		$content = ob_get_contents();
		ob_end_clean();

		include $this->layoutPath . $this->layout;
	}

}