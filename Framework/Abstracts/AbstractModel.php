<?php

namespace Framework\Abstracts;

use Framework\Db;
use Framework\Exceptions\DbException;
use Framework\Exceptions\ModelException;
use Framework\Helpers\Traits\FillTrait;

/**
 * Class AbstractModel
 * @package Framework\Abstracts
 */
abstract class AbstractModel
{
	use FillTrait;

	const table = 'abstract';
	const pk = 'abstract';

	/**
	 * @throws ModelException
	 */
	public function __construct()
	{
		if (static::table == 'abstract' || static::pk == 'abstract')
		{
			throw new ModelException();
		}
	}

	/**
	 * @param array|null $where
	 * @return array
	 */
	public static function getMany(array $where = null)
	{
		$records = Db::getInstance()->getMany(static::table, $where);
		$data = [];
		foreach ($records as $record)
		{
			$data[] =& static::makeUpRecord($record);
		}

		return $data;
	}

	/**
	 * using because of \DateTime fields
	 *
	 * @param array|null $record
	 * @return static
	 */
	private static function makeUpRecord(array $record = null)
	{
		$obj = new static();
		$obj->fillFromArray($record);

		return $obj;
	}

	/**
	 * @param $where
	 * @return static
	 */
	public static function getOne($where = null)
	{
		$record = Db::getInstance()->getOne(static::table, $where);
		if ($record)
		{
			$record = static::makeUpRecord($record);
		}

		return $record;
	}

	/**
	 * @param $where
	 * @return int
	 */
	public static function count($where)
	{
		try
		{
			$res = Db::getInstance()->count(static::table, $where);
		}
		catch (\Exception $e)
		{
			throw new DbException($e->getMessage(), null, $e);
		}

		return $res;
	}

	/**
	 * @return bool
	 */
	public function insert()
	{
		try
		{
			$res = Db::getInstance()->insert(static::table, $this);
		}
		catch (\Exception $e)
		{
			throw new DbException($e->getMessage(), null, $e);
		}

		return $res;
	}

	/**
	 * @return bool
	 */
	public function update()
	{
		try
		{
			$res = Db::getInstance()->update(static::table, $this, [static::pk => $this->{static::pk}]);
		}
		catch (\Exception $e)
		{
			throw new DbException($e->getMessage(), null, $e);
		}

		return $res;
	}

	/**
	 * @return bool
	 */
	public function delete()
	{
		try
		{
			$res = Db::getInstance()->delete(static::table, [static::pk => $this->{static::pk}]);
		}
		catch (\Exception $e)
		{
			throw new DbException($e->getMessage(), null, $e);
		}

		return $res;
	}

}