<?php

namespace Framework\Abstracts;

use Framework\Engine;
use Framework\Helpers\Traits\FillTrait;

/**
 * Class AbstractModel
 * @package Framework\Abstracts
 */
abstract class AbstractForm
{
	use FillTrait;

	protected $errors;

	/**
	 * @return bool
	 */
	public function hasErrors()
	{
		return !empty($this->errors);
	}

	/**
	 * @return mixed
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	/**
	 * @return bool
	 */
	public function loadFromPost()
	{
		$request = Engine::getInstance()->request();
		if ($request->isPost())
		{
			$this->fillFromArray($request->post());
			$this->fillFromArray($request->files());

			return true;
		}

		return false;
	}

	/**
	 * @return AbstractModel
	 */
	abstract public function getModel();

	/**
	 * @return bool
	 */
	public function validate()
	{
		$formValidators = $this->validators();

		$result = true;
		foreach ($formValidators as $field => $fieldValidators)
		{
			if (property_exists($this, $field))
			{
				$res = true;
				/* @var $validator AbstractValidator */
				foreach ($fieldValidators as $validator)
				{
					if ($res)
					{
						$res = $validator->validate($this->$field);
						if (!$res)
						{
							$this->addErrors($field, $validator->validationErrorMessage($field));
							$result = false;
						}
					}
				}
			}
		}

		return $result;
	}

	/**
	 * @return array
	 */
	protected function validators()
	{
		return [];
	}

	/**
	 * @param $field
	 * @param $error
	 */
	public function addErrors($field = null, $error)
	{
		if (property_exists($this, $field) || !$field)
		{
			$this->errors[$field] = $error;
		}
	}

	/**
	 * @param AbstractModel $model
	 */
	protected function createFromModel(AbstractModel $model)
	{
		foreach ($model as $field => $value)
		{
			$this->$field = null;
		}
	}
}