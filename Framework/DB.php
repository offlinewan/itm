<?php
namespace Framework;

use Framework\Abstracts\AbstractModel;
use Framework\Helpers\Traits\SingletonTrait;

/**
 * Class DB
 * @package Framework
 */
class DB
{
	protected $connection;
	protected $class = null, $lastError;

	use SingletonTrait;

	/**
	 *
	 */
	protected function __construct()
	{
		$dsn = sprintf('mysql:dbname=%s;host=%s', DB_DB, DB_URL);
		$this->connection = new \PDO($dsn, DB_USR, DB_PWD);
		$this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
	}

	/**
	 * @param $className
	 * @return $this
	 */
	public function setClass($className)
	{
		$this->class = $className;

		return $this;
	}

	/**
	 * @param $table
	 * @param null $where
	 * @return array
	 */
	public function getOne($table, $where = null)
	{
		$stmt = $this->_exec($table, $where);
		if ($this->class)
		{
			$data = $stmt->fetch(\PDO::FETCH_CLASS, $this->class);
		}
		else
		{
			$data = $stmt->fetch(\PDO::FETCH_ASSOC);
		}
		$this->class = null;

		return $data;
	}

	/**
	 * @param $table
	 * @param $where
	 * @return \PDOStatement
	 */
	private function _exec($table, $where)
	{
		$q = sprintf("SELECT * FROM %s %s", $table, $this->prepareWhereString($where));
		$stmt = $this->connection->prepare($q);

		$this->prepareWhereStms($where, $stmt);
		$stmt->execute();

		return $stmt;
	}

	/***
	 * @param array|null $where
	 * @return string
	 */
	protected function prepareWhereString(array $where = null)
	{
		if (empty($where))
		{
			return '';
		}

		$whereArr = [];
		foreach ($where as $field => $value)
		{
			$whereArr[] = sprintf(' %1$s = :%1$s ', $field);
		}

		return ' WHERE ' . implode(' AND ', $whereArr);
	}

	/**
	 * @param array|null $where
	 * @param \PDOStatement $stmt
	 */
	protected function prepareWhereStms(array $where = null, \PDOStatement &$stmt)
	{
		if (empty($where))
		{
			return;
		}

		foreach ($where as $field => $value)
		{
			$stmt->bindValue(sprintf(':%s', $field), self::prepareValue($value));
		}
	}

	/**
	 * @param $table
	 * @param null $where
	 * @return array
	 */
	public function getMany($table, $where = null)
	{
		$stmt = $this->_exec($table, $where);
		if ($this->class)
		{
			$data = $stmt->fetchAll(\PDO::FETCH_CLASS, $this->class);
		}
		else
		{
			$data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		}
		$this->class = null;

		return $data;
	}

	/**
	 * @param $table
	 * @param null $where
	 * @return int
	 */
	public function count($table, $where = null)
	{
		$stmt = $this->_exec($table, $where);
		$stmt->rowCount();

		return $stmt->rowCount();
	}

	/**
	 * @param $table
	 * @param $data
	 * @return bool
	 */
	public function insert($table, AbstractModel &$data)
	{
		$fields = [];
		$values = [];
		foreach ($data as $field => $value)
		{
			if ($value !== null)
			{
				$fields[] = '`' . $field . '`';
				$values[] = sprintf(':%1$s', $field);
			}
		}
		$q = sprintf("INSERT INTO `%s` (%s) VALUES (%s)", $table, implode(',', $fields), implode(',', $values));
		$stmt = $this->connection->prepare($q);
		foreach ($data as $field => $value)
		{
			if ($value !== null)
			{
				$stmt->bindValue(':' . $field, self::prepareValue($value));
			}
		}

		$res = $stmt->execute();
		$data->{$data::pk} = $this->connection->lastInsertId();

		return $res;
	}

	/**
	 * @param $table
	 * @param AbstractModel $data
	 * @param $where
	 * @return bool
	 */
	public function update($table, AbstractModel $data, array $where)
	{
		$values = [];
		foreach ($data as $field => $value)
		{
			$values[] = sprintf(' `%1$s` = :%1$s', $field);
		}
		$q = sprintf("UPDATE `%s` SET %s %s", $table, implode(',', $values), $this->prepareWhereString($where));

		$stmt = $this->connection->prepare($q);
		foreach ($data as $field => $value)
		{
			$stmt->bindValue(':' . $field, self::prepareValue($value));
		}

		return $stmt->execute();
	}


	/**
	 * @return string
	 */
	public function getLastError()
	{
		return $this->lastError;
	}


	/**
	 * @param $table
	 * @param $where
	 * @return bool
	 */
	public function delete($table, array $where)
	{
		$q = sprintf("DELETE FROM `%s` %s", $table, $this->prepareWhereString($where));
		$stmt = $this->connection->prepare($q);
		foreach ($where as $field => $value)
		{
			$stmt->bindValue(':' . $field, self::prepareValue($value));
		}

		return $stmt->execute();
	}

	/**
	 * @param $value
	 * @return string
	 */
	private static function prepareValue($value)
	{
		if (is_object($value) && $value instanceof \DateTime)
		{
			return $value->format('Y-m-d H:i:s');
		}

		return $value;
	}
}