<?php
namespace Framework\Extensions;
/**
 * Class Session
 * @package Framework\Extension
 */
class Session
{
	protected $container;

	/**
	 *
	 */
	public function __construct()
	{
		$this->container = $_SESSION;
		$_SESSION = null;
	}

	/**
	 * @param $key
	 * @return null
	 */
	public function get($key)
	{
		$val = null;
		if (array_key_exists($key, $this->container))
		{
			$val = $this->container[$key];
		}

		return $val;
	}

	/**
	 * @param $key
	 * @param $val
	 */
	public function set($key, $val)
	{
		$this->container[$key] = $val;
	}

	/**
	 * @param $key
	 */
	public function delete($key)
	{
		unset($this->container[$key]);
	}

	public function __destruct()
	{
		$_SESSION = $this->container;
	}
}