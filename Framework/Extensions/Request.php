<?php
namespace Framework\Extensions;
/**
 * Class Request
 * @package Framework\Extension
 */
class Request
{
	protected $get = [];
	protected $post = [];
	protected $files = [];
	protected $request = [];

	/**
	 *
	 */
	public function __construct()
	{
		$this->get = $_GET;
		$this->post = $_POST;
		$this->files = $_FILES;
		$this->request = $_REQUEST;

		$_GET = null;
		$_POST = null;
		$_FILES = null;
		$_REQUEST = null;
	}

	/**
	 * @return bool
	 */
	public function isPost()
	{
		return $_SERVER['REQUEST_METHOD'] === 'POST';
	}

	/**
	 * @param $key
	 * @return mixed
	 */
	public function get($key)
	{
		$val = null;
		if (array_key_exists($key, $this->get))
		{
			$val = $this->get[$key];
		}

		return $val;
	}

	/**
	 * @param $key
	 * @return mixed
	 */
	public function request($key)
	{
		$val = null;
		if (array_key_exists($key, $this->request))
		{
			$val = $this->request[$key];
		}

		return $val;
	}

	/**
	 * @param $key
	 * @return mixed
	 */
	public function post($key = null)
	{
		$val = [];
		if ($key === null)
		{
			$val = $this->post;
		}
		else
		{
			if (array_key_exists($key, $this->post))
			{
				$val = $this->post[$key];
			}
		}

		return $val;
	}

	/**
	 * @param $key
	 * @return mixed
	 */
	public function files($key = null)
	{
		$val = [];
		if ($key === null)
		{
			$val = $this->files;
		}
		else
		{
			if (array_key_exists($key, $this->files))
			{
				$val = $this->files[$key];
			}
		}

		return $val;
	}
}