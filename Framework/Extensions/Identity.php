<?php
namespace Framework\Extensions;

use Framework\Abstracts\IdentityInterface;

/**
 * Class Identity
 * @package Framework\Extension
 */
class Identity
{
	const SESSION_KEY = 'identity';

	protected $session;
	/* @var $identity IdentityInterface */
	protected $identity;

	/**
	 * @param Session $session
	 */
	public function __construct(Session $session)
	{
		$this->session = &$session;
		if ($identity = $this->session->get(self::SESSION_KEY))
		{
			$this->set($identity);
		}
	}

	/**
	 * @param IdentityInterface $user
	 */
	public function set(IdentityInterface $user)
	{
		$this->identity = $user;
	}

	/**
	 * @return bool
	 */
	public function isGuest()
	{
		return empty($this->identity);
	}

	/**
	 * @param IdentityInterface $user
	 */
	public function login(IdentityInterface $user)
	{
		$this->session->set(self::SESSION_KEY, $user);
		$this->set($user);
	}

	public function logout()
	{
		$this->session->delete(self::SESSION_KEY);
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->get()->getID();
	}

	/**
	 * @return IdentityInterface
	 */
	public function get()
	{
		return $this->identity;
	}
}