<?php
namespace Framework\Exceptions;
/**
 * Class ModelException
 * @package Framework\Exceptions
 */
class ModelException extends \Exception
{
}