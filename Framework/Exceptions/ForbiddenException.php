<?php
namespace Framework\Exceptions;
/**
 * Class NotFoundException
 * @package Framework\Exceptions
 */
class ForbiddenException extends \Exception
{
}