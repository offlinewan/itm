<?php
namespace Framework;

use Framework\Abstracts\AbstractController;
use Framework\Exceptions\NotFoundException;
use Framework\Extensions\Identity;
use Framework\Extensions\Request;
use Framework\Extensions\Session;
use Framework\Helpers\Traits\SingletonTrait;

/**
 * Class Engine
 * @package Framework
 */
final class Engine
{
	use SingletonTrait;

	protected $request, $session, $identity;
	protected $controller, $action;

	/**
	 *
	 */
	protected function __construct()
	{
		session_start();
		$this->request = new Request();
		$this->session = new Session();
		$this->identity = new Identity($this->session);
	}

	/**
	 * @return Request
	 */
	public function request()
	{
		return $this->request;
	}

	/**
	 * @return Session
	 */
	public function session()
	{
		return $this->session;
	}

	/**
	 * @return Identity
	 */
	public function identity()
	{
		return $this->identity;
	}

	/**
	 * @param AbstractController|null $controller
	 * @param null $action
	 * @throws NotFoundException
	 */
	public function processController(AbstractController $controller = null, $action = null)
	{
		if (empty($action))
		{
			$action = 'Index';
		}

		$method = 'action' . $action;

		if (!method_exists($controller, $method))
		{
			throw new NotFoundException();
		}

		$controller->addToTemplate('controller', str_replace('Controller', '', end(explode('\\', get_class($controller)))));
		$controller->addToTemplate('action', $action);
		$controller->$method();
	}
}