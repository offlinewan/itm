<?php
namespace Framework\Validators;

use Framework\Abstracts\AbstractValidator;

/**
 * Class NotEmptyValidator
 * @package Framework\Validators
 */
class NotEmptyValidator extends AbstractValidator
{
	/**
	 * @param $field
	 * @return string
	 */
	public function validationErrorMessage($field)
	{
		return sprintf("%s field can not be empty.", $field);
	}

	/**
	 * @param $subject
	 * @return bool
	 */
	protected function rule($subject)
	{
		return !empty($subject);
	}
}