<?php

namespace Framework\Validators;

use Framework\Abstracts\AbstractValidator;

/**
 * Class FileSizeValidator
 * @package FileSizeValidator
 */
class FileSizeValidator extends AbstractValidator
{
	protected $maxFileSize;

	/**
	 * @param $size
	 */
	public function __construct($size)
	{
		$this->maxFileSize = $size;
	}

	/**
	 * @param $field
	 * @return string
	 */
	public function validationErrorMessage($field)
	{
		return sprintf("Size of %s file must less then %s MB.", $field, round($this->maxFileSize / 1024 / 1024));
	}

	/**
	 * @param $file
	 * @return bool
	 */
	protected function rule($file)
	{
		$size = filesize($file['tmp_name']);

		return $size > 0 && $size < $this->maxFileSize;
	}
}