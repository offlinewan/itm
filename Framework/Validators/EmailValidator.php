<?php
namespace Framework\Validators;

use Framework\Abstracts\AbstractValidator;

/**
 * Class EmailValidator
 * @package Framework\Validators
 */
class EmailValidator extends AbstractValidator
{
	/**
	 * @param $subject
	 * @return bool
	 */
	protected function rule($subject)
	{
		if (empty($subject))
		{
			return $result = true;
		}
		else
		{
			$result = filter_var($subject, FILTER_VALIDATE_EMAIL);
		}

		return $result;
	}
}