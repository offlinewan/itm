<?php
namespace Framework\Validators;

use Framework\Abstracts\AbstractValidator;

/**
 * Class LoginValidator
 * @package Framework\Validators
 */
class LengthValidator extends AbstractValidator
{
	protected $minLen, $maxLen;

	/**
	 * @param null $minLen
	 * @param null $maxLen
	 */
	public function __construct($minLen = null, $maxLen = null)
	{
		$this->maxLen = $maxLen;
		$this->minLen = $minLen;
	}

	/**
	 * @param $field
	 * @return string
	 */
	public function validationErrorMessage($field)
	{
		return sprintf("Length of %s field must be from %s to %s chars.", $field, $this->minLen, $this->maxLen);
	}

	/**
	 * @param $subject
	 * @return bool
	 */
	protected function rule($subject)
	{
		$passed = 0;
		if (!empty($this->maxLen))
		{
			$passed += (int)(strlen((string)$subject) <= $this->maxLen);
		}
		if (!empty($this->minLen))
		{
			$passed += (int)(strlen((string)$subject) >= $this->minLen);
		}

		$result = $passed == ((int)!empty($this->maxLen) + (int)!empty($this->minLen));

		return $result;
	}

}