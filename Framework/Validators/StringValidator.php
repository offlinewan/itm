<?php
namespace Framework\Validators;

use Framework\Abstracts\AbstractValidator;

/**
 * Class StringValidator
 * @package Framework\Validators
 */
class StringValidator extends AbstractValidator
{
	/**
	 * @param $subject
	 * @return bool
	 */
	protected function rule($subject)
	{
		$pattern = '/^[A-Za-z�-��-�]+$/';
		$result = preg_match($pattern, $subject);

		return $result;
	}
}