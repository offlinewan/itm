<?php
namespace Framework\Validators;

use Framework\Abstracts\AbstractModel;
use Framework\Abstracts\AbstractValidator;

/**
 * Class UniqueValidator
 * @package Framework\Validators
 */
class UniqueValidator extends AbstractValidator
{
	protected $model, $field;

	/**
	 * @param $model
	 * @param $field
	 */
	public function __construct($model, $field)
	{
		$this->model = $model;
		$this->field = $field;
	}

	/**
	 * @param $source
	 * @return bool
	 */
	public function rule($source)
	{
		/* @var $model AbstractModel */
		$model = $this->model;
		$data = $model::getOne([$this->field => $source]);

		return empty($data);
	}

	/**
	 * @param $field
	 * @return string
	 */
	public function validationErrorMessage($field)
	{
		return sprintf("Sorry, this %s already taken.", $field);
	}

}