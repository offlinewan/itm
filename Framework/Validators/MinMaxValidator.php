<?php
namespace Framework\Validators;

use Framework\Abstracts\AbstractValidator;

/**
 * Class LoginValidator
 * @package Framework\Validators
 */
class MinMaxValidator extends AbstractValidator
{
	protected $min, $max;

	/**
	 * @param null $min
	 * @param null $max
	 */
	public function __construct($min = null, $max = null)
	{
		$this->max = (int)$max;
		$this->min = (int)$min;
	}

	/**
	 * @param $field
	 * @return string
	 */
	public function validationErrorMessage($field)
	{
		return sprintf("Value of %s field must be from %s to %s.", $field, $this->min, $this->max);
	}

	/**
	 * @param $subject
	 * @return bool
	 */
	protected function rule($subject)
	{
		$result = true;
		if ($subject !== null)
		{
			$passed = 0;
			if (!empty($this->max))
			{
				$passed += (int)((int)$subject <= $this->max);
			}
			if (!empty($this->min))
			{
				$passed += (int)((int)$subject >= $this->min);
			}

			$result = $passed == ((int)!empty($this->max) + (int)!empty($this->min));
		}

		return $result;
	}

}