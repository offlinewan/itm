<?php
namespace Framework\Validators;

use Framework\Abstracts\AbstractValidator;

/**
 * Class LoginValidator
 * @package Framework\Validators
 */
class LoginValidator extends AbstractValidator
{
	/**
	 * @param $subject
	 * @return bool
	 */
	protected function rule($subject)
	{
		if (empty((string)$subject))
		{
			$result = true;
		}
		else
		{
			$pattern = '/^[A-Za-z][A-Za-z0-9]+$/';
			$result = preg_match($pattern, $subject);
		}

		return $result;
	}
}